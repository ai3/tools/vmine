package vmine

import (
	"bytes"
	"crypto/md5"
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"text/template"
	"time"
)

func firstAddr(ipnet *net.IPNet) net.IP {
	tmp := make([]byte, len(ipnet.IP))
	copy(tmp, ipnet.IP)
	tmp[len(tmp)-1]++
	return net.IP(tmp)
}

func randomMAC() string {
	var b [3]byte
	_, err := rand.Read(b[:])
	if err != nil {
		panic(err)
	}
	return fmt.Sprintf("02:00:00:%02X:%02X:%02X", b[0], b[1], b[2])
}

const idLen = 2

// Generate a unique ID. These are small to avoid having hitting the
// network interface name length limit, which is very low. Shouldn't
// matter much as they are ephemeral.
func uniqueID() string {
	var b [idLen]byte
	_, err := io.ReadFull(rand.Reader, b[:])
	if err != nil {
		panic(err)
	}
	return hex.EncodeToString(b[:])
}

// Generate a truncated hash of a string. For the same reason outlined
// above, we want very short unique identifiers.
func truncatedHashID(s string) string {
	h := md5.New()
	h.Write([]byte(s))
	b := h.Sum(nil)
	return hex.EncodeToString(b[:idLen])
}

func getBridgeDevice(id string) string {
	return fmt.Sprintf("vmine-%s-br", id)
}

func getTAPDevice(id string, idx int) string {
	return fmt.Sprintf("vmine-%s-t%d", id, idx)
}

// Return the list of networks used by all the local interfaces.
func listLocalNetworks() []*net.IPNet {
	ifs, err := net.Interfaces()
	if err != nil {
		log.Printf("warning: could not list local interfaces: %v", err)
		return nil
	}

	var out []*net.IPNet
	for _, ifi := range ifs {
		addrs, _ := ifi.Addrs()
		for _, addr := range addrs {
			if _, ipnet, err := net.ParseCIDR(addr.String()); err == nil {
				out = append(out, ipnet)
			}
		}
	}
	return out
}

// Duration is a time.Duration that can be unmarshaled from JSON using
// ParseDuration, supporting human-friendly duration strings.
type Duration time.Duration

func (d Duration) MarshalJSON() ([]byte, error) {
	return json.Marshal(time.Duration(d).String())
}

func (d *Duration) UnmarshalJSON(b []byte) error {
	var v interface{}
	if err := json.Unmarshal(b, &v); err != nil {
		return err
	}
	switch value := v.(type) {
	case float64:
		*d = Duration(time.Duration(value))
		return nil
	case string:
		tmp, err := time.ParseDuration(value)
		if err != nil {
			return err
		}
		*d = Duration(tmp)
		return nil
	default:
		return errors.New("invalid duration")
	}
}

// Render a text template with the specified parameters. Errors will
// cause a panic.
func renderTemplate(tpl string, params interface{}) string {
	t, err := template.New("").Parse(tpl)
	if err != nil {
		panic(err)
	}
	var buf bytes.Buffer
	if err := t.Execute(&buf, params); err != nil {
		panic(err)
	}
	return buf.String()
}
