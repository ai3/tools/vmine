module git.autistici.org/pipelines/tools/vmine

go 1.19

require (
	golang.org/x/sync v0.11.0
	gopkg.in/yaml.v3 v3.0.1
)
