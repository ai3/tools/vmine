#!/bin/bash -x
#
# Upload a tarball to the Package Registry.
#
# We'd like to keep just the latest version of each of the packages in
# the Package Registry for this project, since they're big. Until
# https://gitlab.com/gitlab-org/gitlab/-/issues/300969 is fixed, we
# have to resort to tricks: we delete the existing package first, and
# create it anew every time. The public URLs will stay the same even
# if the internal IDs keep changing.
#

base_url="${CI_SERVER_URL}/api/v4/projects/${CI_PROJECT_ID}"

package_name="$1"
package_version="$2"
tarball_path="$3"

# Find the existing package, if any, matching name and version.
old_id=$(curl -s -H "JOB-TOKEN: $CI_JOB_TOKEN" ${base_url}/packages \
             | jq "map(select(.name == \"${package_name}\" and .version == \"${package_version}\")) | .[].id")
if [ -n "$old_id" ]; then
    echo "found existing package ${old_id} for ${package_name}/${package_version}"
    curl -s -H "JOB-TOKEN: $CI_JOB_TOKEN" -X DELETE ${base_url}/packages/${old_id}
fi

# Upload the new package.
echo "uploading package ${package_name}/${package_version}"
curl -s -H "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${tarball_path} ${base_url}/packages/generic/${package_name}/${package_version}/${package_name}.tar

exit 0
