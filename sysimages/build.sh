#!/bin/sh
#
# Build a base Debian QEMU QCOW2 image.
#

output="$1"
if [ -z "$output" ]; then
    echo "Usage: $0 <output-tar-file>" >&2
    exit 2
fi

bindir=$(realpath $(dirname "$0"))

tempdir=$(mktemp -d ./tmp.XXXXXXXX)
trap "rm -fr ${tempdir}" EXIT INT TERM

set -eu

sed -e "s,@@DIST@@,${DIST},g" \
    -e "s,@@SIZE@@,${SIZE:-20G},g" \
    -e "s,@@BINDIR@@,${bindir},g" \
    < ${bindir}/template.yml \
    > ${tempdir}/vmdb2.yml

vmdb2 --verbose --image ${tempdir}/rootfs.img ${tempdir}/vmdb2.yml

${bindir}/extract-kernel.sh ${tempdir}/rootfs.img ${tempdir}/

echo "Raw image:"
ls -l ${tempdir}/rootfs.img

echo "Compressing to QCOW2..."
mv ${tempdir}/rootfs.img ${tempdir}/rootfs.img.orig
qemu-img convert -c -O qcow2 ${tempdir}/rootfs.img.orig ${tempdir}/rootfs.img
rm -f ${tempdir}/rootfs.img.orig
rm -f ${tempdir}/vmdb2.yml

echo "QCOW2 image:"
ls -l ${tempdir}/rootfs.img

echo "Creating archive..."
tar -c -f ${output} -C ${tempdir} .
